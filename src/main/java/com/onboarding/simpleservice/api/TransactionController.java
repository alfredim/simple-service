package com.onboarding.simpleservice.api;

import com.onboarding.simpleservice.model.Transaction;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.onboarding.simpleservice.route.TransactionRoute.TRANSACTIONS_ROUTE;

/**
 * Open Bank Mock API for Transactions
 */
@RestController
@RequestMapping(value = "transactions", produces = "application/json")
public class TransactionController {

    @EndpointInject(uri = TRANSACTIONS_ROUTE)
    private ProducerTemplate producerTemplate;

    @EndpointInject(uri = "one-transaction")
    private ProducerTemplate oneTransaction;

    @GetMapping
    public List<Transaction> getTransactions() {

        return (List<Transaction>) producerTemplate.requestBody(null);
    }
}
