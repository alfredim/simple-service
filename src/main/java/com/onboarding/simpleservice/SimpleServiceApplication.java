package com.onboarding.simpleservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.onboarding.simpleservice")
public class SimpleServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(SimpleServiceApplication.class, args);
    }

}
