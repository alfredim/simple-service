package com.onboarding.simpleservice.processor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onboarding.simpleservice.model.Transaction;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Transactions transformer.
 */
@Component
public class TransactionProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransactionProcessor.class);
    private static final String NO_VALUE = null; // change to make the null as "" depending on the contract

    private ObjectMapper objectMapper;

    @Autowired
    public TransactionProcessor(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void process(Exchange exchange) throws Exception {

        LOGGER.info("doStuff took INFO - <CSV>002</CSV> ");
        LOGGER.info("doStuff took INFO - <username>NL4002</username> ");
        LOGGER.info("JSON expression = {\"creditCard\": \"VISA\",\"CreditCardNo\":\"1234567898\"}");
        LOGGER.info("JSON expression = {\"password\": \"password\",\"memorableWord\":\"sensitive\"}");

        JsonNode nodeBody = objectMapper.readValue(exchange.getIn().getBody(String.class), JsonNode.class);
        JsonNode obpTransactions = nodeBody.get("transactions");
        List<Transaction> transactionList = new ArrayList<>();
        for (JsonNode node : obpTransactions) {
            Transaction dto = new Transaction();

            dto.setId(node.at("/id").asText(NO_VALUE));
            dto.setAccountId(node.at("/this_account/id").asText(NO_VALUE));
            dto.setCounterpartyAccount(node.at("/other_account/number").asText(NO_VALUE));
            dto.setCounterpartyName(node.at("/other_account/holder/name").asText(NO_VALUE));
            dto.setCounterPartyLogoPath(node.at("/other_account/metadata/image_URL").asText(NO_VALUE));
            dto.setInstructedAmount(node.at("/details/value/amount").asText(NO_VALUE));
            dto.setInstructedCurrency(node.at("/details/value/currency").asText(NO_VALUE));
            dto.setTransactionAmount(node.at("/details/value/amount").asText(NO_VALUE));
            dto.setTransactionCurrency(node.at("/details/value/currency").asText(NO_VALUE));
            dto.setTransactionType(node.at("/details/type").asText(NO_VALUE));
            dto.setDescription(node.at("/details/description").asText(NO_VALUE));

            transactionList.add(dto);
        }

        exchange.getOut().setBody(transactionList);
    }

}