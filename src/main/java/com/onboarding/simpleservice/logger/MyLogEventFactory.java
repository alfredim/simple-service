package com.onboarding.simpleservice.logger;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.core.impl.LogEventFactory;
import org.apache.logging.log4j.message.Message;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class MyLogEventFactory implements LogEventFactory {

    private Properties prop = new Properties();

    @Override
    public LogEvent createEvent(String loggerName, Marker marker, String fqcn, Level level, Message message, List<Property> properties, Throwable t) {

        Message masked = mask(message);
        return new Log4jLogEvent(loggerName, marker, fqcn, level, masked, properties, t);
    }

    private Message mask(Message message) {

        if (prop.isEmpty()) {
            try (InputStream input = new FileInputStream("./src/main/resources/maskingKeys.properties")) {
                prop = new Properties();
                // load properties from project root folder
                prop.load(input);
            } catch (IOException io) {
                io.printStackTrace();
            }
        }

        return new Message() {
            @Override
            public String getFormattedMessage() {

                String maskedMessage = message.getFormattedMessage();

                for (String key : prop.stringPropertyNames()) {
                    maskedMessage = maskedMessage.replaceAll(getXmlRegex(prop.get(key).toString()), "********");
                    maskedMessage = maskedMessage.replaceAll(getJsonRegex(prop.get(key).toString()), "********");
                }

                return maskedMessage;
            }

            @Override
            public String getFormat() {
                return message.getFormat();
            }

            @Override
            public Object[] getParameters() {
                return message.getParameters();
            }

            @Override
            public Throwable getThrowable() {
                return message.getThrowable();
            }
        };
    }

    private String getXmlRegex(String key) {
        return "(?<=\\<" + key + ">)(.*?)(?=\\<\\/" + key + ">)";
    }

    private String getJsonRegex(String key) {
        return "(?<=\\\"" + key + "\": ?\")(.*?)(?=\\\")";
    }

}

