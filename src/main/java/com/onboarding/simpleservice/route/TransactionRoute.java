package com.onboarding.simpleservice.route;

import com.onboarding.simpleservice.processor.TransactionProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransactionRoute extends RouteBuilder {

    public static final String TRANSACTIONS_ROUTE = "direct:transactions-route";
    public static final String TRANSACTIONS_ROUTE_ID = "capgemini-onboarding-route";
    private static final String URL = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions";


    private TransactionProcessor transactionProcessor;

    @Autowired
    public TransactionRoute(TransactionProcessor transactionProcessor) {
        this.transactionProcessor = transactionProcessor;
    }

    @Override
    public void configure() throws Exception {
        from(TRANSACTIONS_ROUTE)
                .log("Started " + TRANSACTIONS_ROUTE_ID)
                .routeId(TRANSACTIONS_ROUTE_ID)
                .to(URL)
                .process(transactionProcessor);
    }
}
