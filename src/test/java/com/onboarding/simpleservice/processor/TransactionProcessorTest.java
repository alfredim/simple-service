package com.onboarding.simpleservice.processor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onboarding.simpleservice.model.Transaction;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionProcessorTest {

    @InjectMocks
    private TransactionProcessor transactionProcessor;

    @Mock
    private ObjectMapper objectMapper;

    private Exchange ex;
    private static final String response = "\n{\n" +
            "\t\"transactions\": [{\n" +
            "\t\t\"id\": \"897706c1-dcc6-4e70-9d85-8a537c7cbf3e\",\n" +
            "\t\t\"this_account\": {},\n" +
            "\t\t\"other_account\": {},\n" +
            "\t\t\"details\": {},\n" +
            "\t\t\"metadata\": {\n" +
            "\t\t\t\"narrative\": null,\n" +
            "\t\t\t\"comments\": [],\n" +
            "\t\t\t\"tags\": [],\n" +
            "\t\t\t\"images\": [],\n" +
            "\t\t\t\"where\": null\n" +
            "\t\t}\n" +
            "\t}]\n" +
            "}";

    @Before
    public void setup() throws IOException {
        CamelContext ctx = new DefaultCamelContext();
        ex = new DefaultExchange(ctx);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode mockNode = mapper.readValue(response, JsonNode.class);
        when(objectMapper.readValue(response, JsonNode.class)).thenReturn(mockNode);
        ex.getIn().setBody(response);
    }

    @Test
    public void transactionProcessorTest() throws Exception {
        transactionProcessor.process(ex);

        List<Transaction> transactionList = (List<Transaction>) ex.getOut().getBody();
        assertNotNull(transactionList);
    }

}
