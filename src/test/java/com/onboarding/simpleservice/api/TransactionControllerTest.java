package com.onboarding.simpleservice.api;

import com.onboarding.simpleservice.model.Transaction;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransactionControllerTest {

    @InjectMocks
    private TransactionController transactionController;

    @Mock
    private ProducerTemplate producerTemplate;

    @Before
    public void setup() {
        Transaction trx1 = new Transaction();
        trx1.setAccountId("12345");
        Transaction trx2 = new Transaction();
        trx2.setAccountId("54321");

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(trx1);
        transactions.add(trx2);

        when(producerTemplate.requestBody(null)).thenReturn(transactions);
    }

    @Test
    public void getTransactionsTest() {
        List<Transaction> response = transactionController.getTransactions();
        assertEquals("12345", response.get(0).getAccountId());
    }
}
